package com.atlassian.maven.plugins.stash;

import com.atlassian.maven.plugins.amps.FilterPluginDescriptorMojo;

import org.apache.maven.plugins.annotations.Mojo;

/**
 * @since 3.10
 */
@Mojo(name = "filter-plugin-descriptor")
public class StashFilterPluginDescriptorMojo extends FilterPluginDescriptorMojo
{
}
